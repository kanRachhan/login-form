import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import UserRegister from '@/components/UserRegister'
import Stafflist from  '@/components/Stafflist'
import VueCookies from 'vue-cookies'
import Toasted from 'vue-toasted';


import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'

Vue.use(Toasted)
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login,  
    },
    {
      path:'/UserRegister',
      name: 'UserRegister',
      component:UserRegister,
     },
     {
      path:'/Stafflist',
      name: 'Stafflist',
      component:()=>import("@/components/Stafflist.vue"),
     },
     {
      path:'/Listuser',
      name: 'Listuser',
      component:()=>import("@/components/Listuser.vue"),
     },

  ]
})
